class Point {
    constructor(x,  y) {
        this.x = x;
        this.y = y;
    }

    get name() {
        return "Точка"
    }
}

class Line {
    constructor(point1, point2) {
        this.point1 = point1;
        this.point2 = point2;
    }


    get length() {
        return Math.sqrt(Math.pow((this.point1.x - this.point2.x), 2) + Math.pow((this.point1.y - this.point2.y), 2));
    }

    get name() {
        return "Линия";
    }
}

class Triangle {
    constructor(point1, point2, point3) {
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
        this.line1 = new Line(this.point1, this.point2);
        this.line2 = new Line(this.point1, this.point3);
        this.line3 = new Line(this.point2, this.point3);
    }

    get perimetr() {
        return this.line1.length + this.line2.length + this.line3.length;
    }

    get square() {
        let p = this.perimetr*0.5;
        return Math.sqrt(p*(p-this.line1.length)*(p-this.line2.length)*(p-this.line3.length));
    }

    get name() {
        return "Треугольник";
    }
}


class Rectangle {
    constructor(line, deg) {
        this.line1 = line;
        this.deg = deg*Math.PI/180;
        this.length_line2 = Math.tan(this.deg)*this.line1.length;
    }

    get square() {
        return this.line1.length*this.length_line2;
    }

    get perimetr() {
        return (this.line1.length + this.length_line2)*2;
    }

    get name() {
        return 'Прямоугольник';
    }
}


class Circle extends Point {
    constructor(point, radius) {
        super(point.x, point.y);
        this.radius = radius;
    }
    
    get diametre() {
        return this.radius*2;
    }
    
    get square() {
        return Math.pow(this.radius, 2)*Math.PI;
    }
    
    get length() {
        return this.radius*2*Math.PI;
    }

    get name() {
        return "Окружность";
    }

    get coordinateCentre() {
        this.point = new Point(this.x, this.y);
        return this.point;
    }
}

class Ellipse extends Point {
    constructor(point, bigRad, smallRad) {
        super(point.x, point.y);
        this.bigRad = bigRad;
        this.smallRad = smallRad;
    }

    get name() {
        return "Эллипс";
    }

    get square() {
        return this.bigRad*this.smallRad*Math.PI;
    }

    get length() {
        return 2 * Math.PI*Math.sqrt((this.bigRad * this.bigRad + this.smallRad * this.smallRad) / 2);
    }

    get coordinatesCentre() {
        this.point = new Point(this.x, this.y);
        return this.point;
    }
} 

let point1 = new Point(0, 0);
let point2 = new Point(0, 2);
let point3 = new Point(2, 0);
let line1 = new Line(point1, point2);
let triangle1 = new Triangle(point1, point2, point3);
let circle1 = new Circle(point1, 10);
let rectangle1 = new Rectangle(line1, 45);
let ellipse1 = new Ellipse(point1, 15, 10);

console.log(rectangle1.square);


