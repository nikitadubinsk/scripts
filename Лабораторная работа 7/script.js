let obj = [
    {id: 2, name: "Организация", parent_id: null},
    {id: 3, name: "Бухгалтерия", parent_id: 2},
    {id: 6, name: " Отдел охраны", parent_id: 2},
    {id: 7, name: "Караульная служба", parent_id: 6},
    {id: 8, name: "Бюро пропусков", parent_id: 6},
    {id: 12, name: "Патентный отдел", parent_id: 2},
    {id: 13, name: "Лётная служба", parent_id: 2},
    {id: 14, name: "Лётный отряд Боинг 737", parent_id: 13},
    {id: 17, name: "Лётный отряд Боинг 747", parent_id: 13},
    {id: 18, name: "1-ая авиационная эксадрилия Боинг 737", parent_id: 14},
    {id: 19, name: "2-ая авиационная эскадрилия Боинг 737", parent_id: 14},
    {id: 21, name: "Лётно-методический отдел", parent_id: 13},
    {id: 22, name: "Лётно-методический отдел", parent_id: 14},
    {id: 23, name: "Лётно-методический отдел", parent_id: 22},
    {id: 25, name: "Лётно-методический отдел", parent_id: 13}
]

let container = document.querySelector(".container");

function getChild(el) {
    if (el.parent_id === null) {
        let parent = document.createElement("div");
        parent.innerText = el.name;
        parent.classList.add("parent");
        parent.id = el.id;
        container.appendChild(parent);
    } else {
        let child = document.createElement("div");
        child.classList.add("child");
        child.id = el.id;
        child.innerText = el.name;
        let par = document.getElementById(el.parent_id);
        par.appendChild(child);
    }
    if (el.children.length >= 0) {
        el.children.forEach((e) => getChild(e))
    }
}

obj = obj.map((e) => {
    e.children = [];
    return e;
});

obj.forEach((el) => {
    if (el.parent_id !== null) {
        let newEl = obj.find((e) => e.id === el.parent_id);
        newEl.children.push(el);
    }
});
obj = obj.filter((el) => el.parent_id === null);

obj.forEach((e) => getChild(e));