let arr = [];
arr['a'] = 'а';
arr['b'] = 'б';
arr['v'] = 'в';
arr['g'] = 'г';
arr['d'] = 'д';
arr['e'] = 'е';
arr['yo'] = 'ё';
arr['zh'] = 'ж';
arr['z'] = 'з';
arr['i'] = 'и';
arr['j'] = 'й';
arr['k'] = 'к';
arr['l'] = 'л';
arr['m'] = 'м';
arr['n'] = 'н';
arr['o'] = 'о';
arr['p'] = 'п';
arr['r'] = 'р';
arr['s'] = 'с';
arr['t'] = 'т';
arr['u'] = 'у';
arr['f'] = 'ф';
arr['x'] = 'ч';
arr['cz'] = 'ц';
arr['ch'] = 'ц';
arr['sh'] = 'ш';
arr['shh'] = 'щ';
arr['"'] = 'ъ';
arr['y`'] = 'ы';
arr['`'] = 'ь';
arr['e`'] = 'э';
arr['yu'] = 'ю';
arr['ya'] = 'я';

let str = prompt("Введите текст");
let ans = '';

if (str.length > 0) {
	for (let i = 0; i < str.length; i++) {
		let el = str[i];
		let el1;
		let isUpperCase = el.toUpperCase()==el;
		el = el.toLowerCase();
		for (let j in arr) {
			if (el == arr[j]) { 
				el = j;
				if (!!isUpperCase) {
					el = el.toUpperCase();
				}
				el1 = el[0];
				for (let k = 1; k < el.length; k++) {
					el1 += el[k].toLowerCase();
				} 
				break;
			} else  if (!!isUpperCase) {
						el1 = el.toUpperCase();
					} else el1 = el;
		} 
		ans = ans + el1;
	}
} else {
	str = prompt("Пожалуйста, введите текст");
}
console.log(ans);

